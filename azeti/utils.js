/**
 * Copyright (c) 2020 azeti GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

var has = require("lodash.has");
var isFinite = require("lodash.isfinite");
var isObject = require("lodash.isobject");
var isString = require("lodash.isstring");

function isValidHd(payload) {
    return isFinite(payload.processing_level) &&
        isString(payload.sensor_id) && payload.sensor_id.length > 0 &&
        isString(payload.timestamp) && payload.timestamp.length > 0 &&
        has(payload, "value") && !isObject(payload.value);
}

function isValidEvent(payload) {
    return isFinite(payload.severity) && payload.severity >= 0 && payload.severity <= 200 &&
        isString(payload.sensor_id) && payload.sensor_id.length > 0 &&
        isString(payload.timestamp) && payload.timestamp.length > 0 &&
        isString(payload.state) && payload.state.length > 0;
}

module.exports = {isValidHd, isValidEvent};
