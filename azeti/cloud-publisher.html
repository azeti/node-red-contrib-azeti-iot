<!--
  Copyright JS Foundation and other contributors, http://js.foundation
  Modifications Copyright (c) 2020 azeti GmbH
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  http://www.apache.org/licenses/LICENSE-2.0
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
-->

<script type="text/x-red" data-help-name="cloud publisher">
    <p>Collects either historical data and event messages, and deliver them to azeti Cloud.</p>
    <h3>Inputs</h3>
    <dl class="message-properties">
       <dt>payload <span class="property-type">object</span></dt>
       <dd>a JavaScript object representing a historical data or an event object. Invalid
       objects get discarded.</dd>
    </dl>
    <h3>Details</h3>
    <p><code>msg.payload</code> is used as the payload of the published message.
    If it contains an event object, it will be stored for not more than one second and delivered as a bulk message.
    If it contains a historical data object, it will be stored for the configured <code>Upload Interval</code>
    seconds at most and delivered as a bulk message.</p>
    <p>If a message contains more than four (4) objects, it is compressed via Gzip before publishing it to the
    cloud server.</p>
    <p>This node requires an azeti account, and a live connection to azeti Cloud server. It is configured by
    clicking the pencil icon next to the broker dropdown in the properties.</p>
    <p>Several cloud publisher nodes can share the same cloud connection if required.</p>
</script>

<script type="text/x-red" data-template-name="cloud publisher">
    <div class="form-row">
        <label for="node-input-broker"><i class="fa fa-globe"></i> <span>Broker</span></label>
        <input type="text" id="node-input-broker">
    </div>
    <div class="form-row">
        <label for="node-input-uploadInterval" style="width: auto"><i class="fa fa-clock-o"></i> Upload interval (s)</label>
        <input type="number" id="node-input-uploadInterval" min="15" placeholder="Upload interval" style="margin-left:20px; width:65px; ">
    </div>
    <div class="form-row">
        <label for="node-input-name"><i class="fa fa-tag"></i> <span>Name</span></label>
        <input type="text" id="node-input-name" placeholder="Name">
    </div>
</script>

<script type="text/javascript">
    RED.nodes.registerType('cloud publisher',{
        category: 'azeti',
        defaults: {
            name: {value:""},
            broker: {type:"azeti-broker", required:true},
            uploadInterval: {
                value: 60, validate: function (v) {
                    return /^-{0,1}\d+$/.test(v) && v >= 15;
                }
            }
        },
        color:"#e9967a",
        inputs:1,
        outputs:0,
        icon: "bridge.svg",
        align: "right",
        label: function() {
            return this.name||"cloud publisher";
        },
        labelStyle: function() {
            return this.name?"node_label_italic":"";
        }
    });
</script>

<script type="text/html" data-template-name="azeti-broker">
    <div class="form-row node-input-broker">
        <label for="node-config-input-broker"><i class="fa fa-globe"></i> <span>Broker</span></label>
        <input type="text" id="node-config-input-broker" style="width:40%;" placeholder="broker.azeti.net">
        <label for="node-config-input-port" style="margin-left:20px; width:43px; "> <span>Port</span></label>
        <input type="text" id="node-config-input-port" placeholder="8883" style="width:55px">
    </div>
    <div class="form-row">
        <input type="checkbox" id="node-config-input-usetls" style="display: inline-block; width: auto; vertical-align: top;">
        <label for="node-config-input-usetls" style="width: auto">Enable secure (SSL/TLS) connection</label>
    </div>
    <div class="form-row">
        <label for="node-config-input-organizationShortName"><i class="fa fa-tasks"></i> <span>Org. name</span></label>
        <input type="text" id="node-config-input-organizationShortName">
    </div>
    <div class="form-row">
        <label for="node-config-input-serial"><i class="fa fa-cube"></i> <span>Serial</span></label>
        <input type="text" id="node-config-input-serial">
    </div>
    <div class="form-row">
        <label for="node-config-input-user"><i class="fa fa-user"></i> <span>Username</span></label>
        <input type="text" id="node-config-input-user">
    </div>
    <div class="form-row">
        <label for="node-config-input-password"><i class="fa fa-lock"></i> <span>Password</span></label>
        <input type="password" id="node-config-input-password">
    </div>
    <div class="form-row">
        <label for="node-config-input-name"><i class="fa fa-tag"></i> <span>Name</span></label>
        <input type="text" id="node-config-input-name" placeholder="Name">
    </div>
</script>

<script type="text/x-red" data-help-name="azeti-broker">
    <p>Configuration for a connection to azeti Cloud.</p>
    <p>This configuration will create a single connection to azeti Cloud which can
       then be reused by <code>cloud publisher</code> node.</p>
    <p>The correct parameters can be obtained on your azeti account, using the Node-RED setup.</p>

</script>

<script type="text/javascript">
    RED.nodes.registerType('azeti-broker',{
        category: 'config',
        defaults: {
            name: {value:""},
            broker: {value:"",required:true},
            port: {value:8883,required:false,validate:RED.validators.number(true)},
            usetls: {value: false}
        },
        credentials: {
            user: {type:"text", required: true},
            password: {type: "password", required: true},
            organizationShortName: {type:"text", required: true, validate:RED.validators.regex(/^[a-zA-Z0-9_-]*$/)},
            serial: {type:"text", required: true, validate:RED.validators.regex(/^[a-zA-Z0-9_-]*$/)}
        },
        label: function() {
            return this.name ? this.name : "cloud publisher";
        },
        oneditprepare: function () {
            if (typeof this.usetls === 'undefined') {
                this.usetls = false;
                $("#node-config-input-usetls").prop("checked",false);
            }

            function updateTLSOptions() {
                if ($("#node-config-input-usetls").is(':checked')) {
                    $("#node-config-row-tls").show();
                } else {
                    $("#node-config-row-tls").hide();
                }
            }
            updateTLSOptions();
            $("#node-config-input-usetls").on("click",function() {
                updateTLSOptions();
            });

            function updatePortEntry(){
                var disabled = $("#node-config-input-port").prop("disabled");
                if ($("#node-config-input-broker").val().indexOf("://") === -1){
                    if (disabled){
                        $("#node-config-input-port").prop("disabled", false);
                    }
                }
                else {
                    if (!disabled){
                        $("#node-config-input-port").prop("disabled", true);
                    }
                }
            }
            $("#node-config-input-broker").on("change", function() {
                updatePortEntry();
            });
            $("#node-config-input-broker").on( "keyup", function() {
                updatePortEntry();
            });
            setTimeout(updatePortEntry,50);
        }
    });
</script>