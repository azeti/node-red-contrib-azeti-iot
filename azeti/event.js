/**
 * Copyright (c) 2020 azeti GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

module.exports = function (RED) {
    "use strict";
    var has = require("lodash.has");
    var isNil = require("lodash.isnil");
    var isFinite = require("lodash.isfinite");
    var isObject = require("lodash.isobject");
    var isString = require("lodash.isstring");
    var {isValidHd} = require("./utils");

    function parseSeverity(severityField) {
        if (isString(severityField)) {
            switch (severityField.toUpperCase()) {
                case "CRITICAL":
                    return 200;
                case "WARNING":
                    return 100;
                default:
                    return 0;
            }
        } else if (isFinite(severityField)) {
            if (severityField < 0) {
                return 0;
            }
            if (severityField > 200) {
                return 200;
            }
            return severityField;
        } else {
            return 0;
        }
    }

    function getHd(payload) {
        if (isValidHd(payload)) {
            return {
                processing_level: payload.processing_level,
                sensor_id: payload.sensor_id,
                timestamp: payload.timestamp,
                value: payload.value
            };
        }
        return null;
    }

    function EventNode(config) {
        RED.nodes.createNode(this, config);
        var node = this;
        node.on('input', function (msg) {

            var state = config.state;
            var severity = config.severity;
            var outputMessage = config.outputMessage;
            var sensorId = config.sensorId;

            if (!sensorId) {
                sensorId = msg.topic;
            }

            if (!sensorId) {
                node.warn("No sensorId defined on node settings nor via message topic");
                return;
            }

            if (isString(msg.payload)) {
                if (!state) {
                    state = msg.payload;
                }
            } else if (isObject(msg.payload)) {
                if (Array.isArray(msg.payload)) {
                    node.warn("Only strings and StateParam datatypes are supported");
                    return;
                }

                if (!state) {
                    if (!has(msg.payload, "state")) {
                        node.warn("State is not defined on objects neither on node settings");
                        return;
                    }
                    if (!isString(msg.payload.state)) {
                        node.warn("Only strings are accepted on property 'state'");
                        return;
                    }
                    state = msg.payload.state;
                }

                if (isNil(severity) || severity.length === 0) {
                    severity = msg.payload.severity;
                }

                if (!outputMessage) {
                    if (isString(msg.payload.output) && msg.payload.output.length > 0) {
                        outputMessage = msg.payload.output;
                    }
                }
            } else {
                node.warn("Only strings and StateParam datatypes are supported");
                return;
            }

            if (!isNaN(severity)) {
                severity = +severity;
            }
            severity = parseSeverity(severity);

            var hd = getHd(msg.payload);

            msg.topic = sensorId;
            msg.payload = {
                hd: hd !== null ? [hd] : [],
                sensor_id: sensorId,
                state: state,
                severity: severity,
                timestamp: new Date().toISOString()
            };

            if (outputMessage) {
                msg.payload.output = outputMessage;
            }

            node.send(msg);
        });
    }

    RED.nodes.registerType("event", EventNode);
}
