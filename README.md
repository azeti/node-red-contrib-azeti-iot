node-red-contrib-azeti-iot
==========================

node-red-contrib-azeti-iot is a collection of nodes that allow you to connect [Node-RED](http://nodered.org) to [azeti Cloud services](https://azeti.net), allowing you to collect, visualize and analyze sensor data, as well getting notified under customizable circumstances.

# Requirements

To use these nodes, you should have:

* Node-RED v1.0+
* An azeti account (you can register your free account [here](https://cloud.azeti.net/app))

# Installation

The recommended way to install these nodes is by using the Node-RED palette, you can access it on the **top-right menu | Manage Palette**.

Alternatively, you can manually install it with the commands:

```shell
cd ~/.node-red
npm install @azeti/node-red-contrib-azeti-iot
```

# Nodes

* sensor id setter
    * Set the topic of a message as the Sensor ID, useful to label several data sources connected to one historical data and event nodes
* historical data
    * Convert primitive JavaScript data types in historical data objects
* event
    * Creates an event object, according to a message payload or a node properties
* cloud publisher
    * Collects historical data and event messages, and delivers them to azeti Cloud

# Usage

A good starting point is by using the provided example in the repository, under `examples/basic_example.json`. For that you need to do the following steps:

1. Start Node-RED and open the web interface
2. Open the provided example by clicking on the **top-right menu | Import | Examples | node-red-contrib-azeti-iot | basic_example** and click **Import**
3. Double click on the **cloud publisher** node
4. Setup the broker settings with the parameters obtained on your azeti Cloud interface
5. Click Update/Add and Done to update the node
6. Click in the **Deploy** button to deploy the nodes in the running flow
7. You can click in the **inject** nodes to create the respective historical data and events
8. In azeti Cloud you can see the events and historical data received by clicking in the configured Site

# License

Copyright (c) 2020 [azeti GmbH](https://azeti.net)

Release under the [Apache License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0)
